package org.smartboot.maven.plugin.mydalgen.plugins.mybatis.config;

/**
 * @author Seer
 * @version IWalletConfigException.java, v 0.1 2015年7月25日 上午10:27:21 Seer Exp.
 */
public class IWalletConfigException extends Exception {
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 3690192148834105400L;

	/**
	 * Constructor for IWalletConfigException.
	 */
	public IWalletConfigException() {
		super();
	}

	/**
	 * Constructor for IWalletConfigException.
	 */
	public IWalletConfigException(String message) {
		super(message);
	}

	/**
	 * Constructor for IWalletConfigException.
	 */
	public IWalletConfigException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor for IWalletConfigException.
	 */
	public IWalletConfigException(String message, Throwable cause) {
		super(message, cause);
	}
}
