/**
 * www.hipac.cn Inc.
 * Copyright (c) 2004-2020 All Rights Reserved.
 */
package org.smartboot.maven.plugin.mydalgen.plugins.mybatis.config;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * result标签属性配置
 *
 * @author jingtianer
 * @version maven-mydalgen-plugin, v 0.1 2020-12-22 11:08 jingtianer Exp $
 */
public class IWalletResultConfig {

    /** 参数类型 */
    private String javaType;

    /** 泛型类型 */
    private String genericType;

    public IWalletResultConfig(){
        super();
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getGenericType() {
        return genericType;
    }

    public void setGenericType(String genericType) {
        this.genericType = genericType;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
